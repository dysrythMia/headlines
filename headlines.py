
import feedparser
from flask import make_response
from flask import Flask
from flask import render_template
from flask import request
import json
import urllib2
import urllib




app = Flask(__name__)


RSS_FEEDS = {'bbc': 'http://www.basised.com/tucson-north/senior-projects/feed/',
             'cnn': 'http://www.jpost.com/Rss/RssFeedsHeadlines.aspx',
             'fox': 'http://feeds.foxnews.com/foxnews/latest',
             'iol': 'http://www.jpost.com/Rss/RssFeedsIslamicTerrorism'}

WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={}&units=imperial&APPID=99dcde3cbc90fe18647cc4a533008c59"
CURRENCY_URL = "https://openexchangerates.org//api/latest.json?app_id=9c165a6c0367494f912932535b053299"

DEFAULTS = {'publication':'bbc',
                        'city':'London,UK',
                        'currency_from':'GBP',
                        'currency_to':'USD'}


@app.route("/")

def home():
                #get costumized headlines, based o user input or default
                publication = request.args.get('publication')
                if not publication:
                        publication = request.cookies.get("publication")
                        if not publication:
                                publication = DEFAULTS['publication']
                articles = get_news(publication)
                #get customzed weather based on user input or default
                city = request.args.get('city')
                if not city:
                        city = DEFAULTS['city']
                weather = get_weather(city)
                currency_from = request.args.get('currency_from')
                if not currency_from:
                        currency_from = DEFAULTS['currency_from']
                currency_to = request.args.get('currency_to')
                if not currency_to:
                        currency_to = DEFAULTS['currency_to']
                rate = get_rate(currency_from, currency_to)
                response = make_response(render_template('home.html',
                        articles=articles,
                        weather=weather,
                        currency_from=currency_from,
                        currency_to=currency_to,
                        rate=rate,))

                response.set_cookie("publication", publication)
                response.set_cookie("city", city)
                response.set_cookie("currency_from", currency_from)
                response.set_cookie("currency_to", currency_to)
                return response


def get_news(query):
        if not query or query.lower() not in RSS_FEEDS:
                publication = "bbc"
        else:
                publication = query.lower()
        feed = feedparser.parse(RSS_FEEDS[publication])
        return feed['entries']

def get_weather(query):
        query = urllib.quote(query)
        url = WEATHER_URL.format(query)
        data = urllib2.urlopen(url).read()
        parsed = json.loads(data)
        weather = None
        if parsed.get("weather"):
                weather = {'description':
                                        parsed['weather'][0]['description'],
                                        'temperature':parsed['main']['temp'],
                                        'city':parsed['name'],
                                        'country': parsed['sys']['country']
                                        }
        return weather

def get_rate (frm, to):
        all_currency = urllib2.urlopen(CURRENCY_URL).read()

        parsed = json.loads(all_currency).get('rates')
        frm_rate = parsed.get(frm.upper())
        to_rate = parsed.get(to.upper())
        return to_rate/frm_rate

if __name__ == "__main__":
        app.run(port=5000, debug=True)

